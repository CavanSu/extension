//
//  ViewController.swift
//  Device
//
//  Created by CavanSu on 2019/5/15.
//  Copyright © 2019 CavanSu. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("\(DeviceInfo.version)")
        print("\(DeviceInfo.versionName)")
        print("\(DeviceInfo.batteryLevel)")
        print("\(DeviceInfo.batteryStatus.rawValue)")
    }

}

