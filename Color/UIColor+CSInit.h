//
//  UIColor+CSRGB.h
//
//  Created by CavanSu on 17/3/3.
//  Copyright © 2017 CavanSu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CSRGB)
+ (UIColor * _Nonnull)red:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue;
@end

@interface UIColor (Hex)
+ (UIColor * _Nonnull)colorWithHexString:(NSString *)color;
@end
