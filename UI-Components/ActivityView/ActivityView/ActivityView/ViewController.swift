//
//  ViewController.swift
//  ActivityView
//
//  Created by CavanSu on 2019/5/23.
//  Copyright © 2019 CavanSu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    lazy var activity: CSActivityView = {
        let view = CSActivityView(size: 70, superview: self.view)
        view.offsetX = 100;
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func doStartPressed(_ sender: UIButton) {
        sender.isSelected.toggle()
        if sender.isSelected {
            activity.startAnimation()
        } else {
            activity.stopAnimation()
        }
    }
}

