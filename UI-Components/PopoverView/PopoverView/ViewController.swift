//
//  ViewController.swift
//  PopoverView
//
//  Created by CavanSu on 2020/4/3.
//  Copyright © 2020 CavanSu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var button: UIButton!
    
    let popover = Popover(options: [.type(.down),
                                    .blackOverlayColor(UIColor.clear),
                                    .cornerRadius(5.0),
                                    .arrowSize(CGSize(width: 8, height: 4))])
    
    let redView = UIView(frame: CGRect(x: 0, y: 0, width: 190, height: 100))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        popover.strokeColor = UIColor.purple
        popover.arrowColor = UIColor.yellow
        redView.backgroundColor = UIColor.yellow
    }
    
    @IBAction func doButtonPressed(_ sender: UIButton) {
        popover.show(redView, fromView: sender)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
}

