//
//  ViewController.swift
//  Color
//
//  Created by CavanSu on 2019/5/23.
//  Copyright © 2019 CavanSu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var colorView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let image = imageView.image {
            let color = image.pixelColor(atLocation: CGPoint(x: 10, y: 10))
            colorView.backgroundColor = color
        }
    }
}

