//
//  DeviceInfo.swift
//  Device
//
//  Created by CavanSu on 2019/5/15.
//  Copyright © 2019 CavanSu. All rights reserved.
//

import UIKit

class DeviceInfo: NSObject {
    static var version: String {
        #if os(iOS)
        return UIDevice.current.systemVersion
        #else
        let systemVersion = ProcessInfo.processInfo.operatingSystemVersion
        let osVersion = "\(systemVersion.majorVersion).\(systemVersion.minorVersion).\(systemVersion.patchVersion)"
        return osVersion
        #endif
    }
    
    static var owner: String {
        #if os(iOS)
        return UIDevice.current.name
        #else
        var owner = ""
        if let name = Host.current().localizedName {
            owner = name
        }
        return owner
        #endif
    }
    
    static var versionName: String {
        return UIDevice.current.systemName
    }
    
    static var batteryLevel: Float {
        return UIDevice.current.batteryLevel
    }
    
    static var batteryStatus: UIDevice.BatteryState {
        return UIDevice.current.batteryState
    }
}
